<?php

include_once "include/config.php";
include_once "include/func.php";

// ignorelist / blacklist
$ignores = [];
if(isset($_COOKIE['ignorelist'])){
	$ignores = array_unique(explode(" ", $_COOKIE['ignorelist']));
		
	// remove blank query
	while (($key = array_search("", $ignores)) !== false) {
		unset($ignores[$key]);
	}
}

// create bins
$tag_and = [];
$tag_or  = [];
$tag_not = $ignores;

if(array_key_exists('tags', $_GET)){	
	// split tag query into tokens
	$tag_query = array_unique(explode(" ", $_GET['tags']));

	// remove blank query
	while (($key = array_search("", $tag_query)) !== false) {
		unset($tag_query[$key]);
	}
	
	if (count($tag_query) > 0){
	// place tags in their appropriate bins
	foreach ($tag_query as $tag){
			// sanitize each string
			$tag = filter_var($tag, FILTER_SANITIZE_MAGIC_QUOTES);
			// or
			if (substr($tag, 0, 1) == "~" ){
				array_push($tag_or, substr($tag, 1));
			} else
			// not
			if (substr($tag, 0, 1) == "-" ){
				array_push($tag_not, substr($tag, 1));
			// default: and
			} else {
				array_push($tag_and, $tag);
			}
		}
	}
} else {
	// put every tag on the "or" bin
	$stmt = $pdo->prepare('Select name From tags');
	$stmt->execute();
	while ($tag = $stmt->fetch()){
		array_push($tag_or, $tag['name']);
	}
}

// Form our long, long query string
$query_string = "
Select images.id As img_id,
       images.thumb As img_thumb,
       images.title As img_title
       From images
Inner Join imagetags On imagetags.imgid = images.id
Inner Join tags On tags.id = imagetags.tagid
Where images.id In (
    Select imgid From imagetags
    Inner Join tags On tags.id = imagetags.tagid
    Where tags.name In ( ";

if (count($tag_or) == 0){ // only count ands
	$query_string .= to_list_str($tag_and, "'");
} else { // only count ors
	$query_string .= to_list_str($tag_or, "'");
}

$query_string =  $query_string . " ) Group By imagetags.imgid ";

if (count($tag_or) == 0){
	$tac = count($tag_and);
	$query_string .= "Having Count(imagetags.tagid) = $tac ";
}

$query_string .= " )
And images.id Not In (
    Select imgid From imagetags
    Inner Join tags On tags.id = imagetags.tagid
    Where tags.name In ( ";

if (count($tag_not) == 0){
	$query_string .= "'' ";
} else {
// count not
	$query_string .= to_list_str($tag_not, "'");
}

$query_string .= " ) Group By imagetags.imgid ) Group By img_id ";

if(array_key_exists('after', $_GET)){
	$after = filter_var($_GET['after'], FILTER_SANITIZE_NUMBER_INT);
	$after_query = " Where query.img_id > ".$after;
} else {
	// "after" defaults
	$after_query = " Where query.img_id > 0 ";
}

if(array_key_exists('limit', $_GET)){
	$limit = filter_var($_GET['limit'], FILTER_SANITIZE_NUMBER_INT);
	$limit_query = " Limit ".$limit;
} else {
	// "limit" defaults
	//$limit_query = " Limit 25";
	$limit_query = " ";
}

$final_db_query = "Select query.* From ( ".$query_string." ) As query ".$after_query." ".$limit_query;

// prepare contents
$contents = "<div id=\"list\">";

$images = [];
$stmt = $pdo->prepare($final_db_query);

$stmt->execute();
while ($row = $stmt->fetch()){
	array_push($images, $row);
}

foreach ($images as $image){
	$id = $image['img_id'];
	$it = $image['img_thumb'];
	$in = $image['img_title'];
	$contents .= "<div class=\"list-entry\">";
	//$contents .= "<a href=\"show.php?id=$id\"><div class=\"img-wrapper\"><span class=\"img-spacer\"></span><img src=\"$thumbnail_dir$it\" alt=\"$in\"/></div><span class=\"list-entry-title\">$in</span></a>";
	$contents .= "<a href=\"show.php?id=$id\"><div class=\"img-wrapper\"><span class=\"img-spacer\"></span><img src=\"$thumbnail_dir$it\" alt=\"$in\"/></div></a>";
	$contents .= "</div>\n";
}

/*$a = print_r($tag_and, true);
$b = print_r($tag_or, true);
$c = print_r($tag_not, true);
$d = print_r($final_db_query, true);

$contents = "<p>$a</p><p>$b</p><p>$c</p><p>$d</p>";
*/
// render
require "include/render.php";
?>
