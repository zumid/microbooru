<?php

include_once "config.php";
include_once "func.php";

// $title    = page's title
// $contents = what to take center stage

// sanitize before passing
// $title = 

// Header

// create tag array
$gt = $_GET['tags'];
$ga = [];
// TODO: every tag is treated like ANDs for now, need to do OR
// remove blank query
$tag_query = array_unique(explode(" ", $gt));
foreach ($tag_query as $tag){
	// sanitize queries
	$tag = filter_var($tag, FILTER_SANITIZE_MAGIC_QUOTES);
	
	// ignore NOT
	if (substr($tag, 0, 1) == "-" ){}
	else{
	// XXX: querying everything
	$stmt = $pdo->prepare('Select id From tags Where name=?');
	$stmt->execute([$tag]);
	$row = $stmt->fetch();
	
	array_push($ga, $row['id']);
	}
}
while (($key = array_search("", $ga)) !== false) {
	unset($ga[$key]);
}
$gc = count($ga);

echo "
<!DOCTYPE html>
<html>
  <head>
  <title>$title</title>
  <link rel=\"stylesheet\" href=\"$assets_dir"."style.css\" media=\"screen,projection,tv\">
  </head>
  <body>
    <div id=\"header\">
      <h1><a href=\"/\">$site_name</a></h1>
      <ul id=\"navbar\">
        <li><a href=\"/upload.php\">Upload</a></li>
        <li><a href=\"/set_ignore.php\">Ignorelist</a></li>
      </ul>
    </div>
    <div id=\"centerstage\">
      <div id=\"sidebar\">
      <form id=\"search-bar\" action=\"/\" method=\"get\">
      <input id=\"search-tags\" type=\"text\" name=\"tags\" value=\"$gt\" />
      <input type=\"submit\" value=\"Go!\" />
      </form>";

if (isset($sidebar)){
	echo $sidebar;
} else {
	echo "
	      <ul>";
	// sidebar contents
	if ($gc > 0){
		$stmt_ = '
		Select id, name From tags 
		Where id In (
		  Select Distinct tagid From imagetags
		  Where imgid in (
		    Select imgid From imagetags Where tagid In (';
		
		$stmt_ .= to_list_str($ga, "");
		
		$stmt_ .= ')
		    Group By imgid Having Count(tagid)=?
		  )
		) Order By name Asc';
		$stmt = $pdo->prepare($stmt_);
		$stmt->execute([$gc]);
	}
	else {
		$stmt_ = 'Select id, name From tags Order By name Asc';
		$stmt = $pdo->prepare($stmt_);
		$stmt->execute();
	}

	while ($row = $stmt->fetch()){
		$ri = $row['id'];
		$rn = $row['name'];
		
		// number of images with the tag
		$stxt = $pdo->prepare('Select Count(tagid) as tag_count From imagetags Where tagid=?;');
		$stxt->execute([$ri]);
		$tc = $stxt->fetch();
		$tn = $tc['tag_count'];
		
		if (($tn + 0) > 0) {
			$get_params = ['tags' => $gt.' '.$rn];
			$get_query  = http_build_query($get_params);
			
			$get_params_not = ['tags' => $gt.' -'.$rn];
			$get_query_not  = http_build_query($get_params_not);
			
			echo "\n        <li>
			  <a href=\"/?$get_query\">+</a>
			  <a href=\"/?$get_query_not\">-</a>
			  <a>?</a>
			  <a href=\"/?tags=$rn\">$rn</a>
			  <span class=\"tag-count\">$tn</span>
			</li>";
		}
	}

	echo "
	      </ul>";
}
echo "
      </div>
      <div id=\"content\">
      $contents
      </div>
    </div>
    <div id=\"footer\">
      <p>&copy; 2021 Zumi</p>
      <p>A quite basic booru-inspired engine.</p>
    </div>
  </body>
</html>
";


?>
