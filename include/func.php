<?php
// setup db
$squery = "mysql:host=$host;dbname=$db";
$options = [
	 PDO::ATTR_ERRMODE				=> PDO::ERRMODE_EXCEPTION,
	 PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
	 PDO::ATTR_EMULATE_PREPARES	=> false,
];

$pdo = new PDO($squery, $user, $pass, $options);

// general funcs
function to_list_str($array, $quote_char){
	$str = '';
	for ($i = 0; $i < count($array); $i++){
		$el = $array[$i];
		if ($i == count($array)-1 ){ $str .= "$quote_char$el$quote_char"; }
		else{ $str .= "$quote_char$el$quote_char, "; }
	}
	return $str;
}

function generate_random_string($length) {
    $key = '';
    $keys = array_merge(range(0, 9), range('a', 'z'));

    for ($i = 0; $i < $length; $i++) {
        $key .= $keys[array_rand($keys)];
    }

    return $key;
}

function create_thumbnail($source_dir, $thumbnail_dir, $filename, $thumbnail_width) {
	if(preg_match('/[.](jpg)$/', $filename)) {
		$im = imagecreatefromjpeg($source_dir . $filename);
	} else if (preg_match('/[.](gif)$/', $filename)) {
		$im = imagecreatefromgif($source_dir . $filename);
	} else if (preg_match('/[.](png)$/', $filename)) {
		$im = imagecreatefrompng($source_dir . $filename);
	}

	$ox = imagesx($im);
	$oy = imagesy($im);

	$nx = $thumbnail_width;
	$ny = floor($oy * ($thumbnail_width / $ox));

	$nm = imagecreatetruecolor($nx, $ny);

	imagecopyresized($nm, $im, 0,0,0,0,$nx,$ny,$ox,$oy);

	if(!file_exists($thumbnail_dir)) {
		if(!mkdir($thumbnail_dir)) {
			die("There was a problem. Please try again!");
		} 
	}

	imagejpeg($nm, $thumbnail_dir.$filename);
}

// ui funcs
function display_warning($message){
	return '<div class="warning">'.$message.'</div>';
}

function display_confirmation($message){
	return '<div class="confirmation">'.$message.'</div>';
}
?>
