<?php

// DB configs
$host = '127.0.0.1';
$db	= 'mybooru';
$user = 'root';
$pass = 'root';

// directories
$assets_dir = 'assets/';
$fullsize_img_dir = 'img/';
$thumbnail_dir    = 'thumb/';

// site settings
$site_name = "microbooru";
$thumbnail_width = 150;

?>
