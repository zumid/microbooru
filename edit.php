<?php

include_once "include/config.php";
include_once "include/func.php";

if (isset($_POST['id'])){
	// TODO: error out when no tag
	// TODO: clean up code and validation
	
	// get stuff
	$id = filter_var($_POST['id'], FILTER_SANITIZE_NUMBER_INT);
	$tt = $_POST['title'];
	$ds = $_POST['desc'];
	$pp = $_POST['tags'];
	
	// blank out sidebar
	$contents = "<style>#sidebar{display: none;}</style>";
	$sidebar = "";
	
	// update title and desc directly
	$stmt = $pdo->prepare('Update images Set title=?, description=? Where id=?');
	$stmt->execute([$tt, $ds, $id]);
	
// update tags
// delete all the img tags
	$stmt = $pdo->prepare('Delete From imagetags Where imgid=?');
	$stmt->execute([$id]);
// insert new ones in its place
	// split tag query into tokens
	$tags = array_unique(explode(" ", $pp));
	
	// remove blank query
	while (($key = array_search("", $tags)) !== false) {unset($tags[$key]);}
	
	foreach ($tags as $tag){
		// sanitize each string
		$tag = filter_var($tag, FILTER_SANITIZE_MAGIC_QUOTES);
		// ignore all the ones with special characters
		if (substr($tag, 0, 1) == "~" ){}
		else if (substr($tag, 0, 1) == "-" ){}
		else {
		// process tag
			$stmt = $pdo->prepare('Select id From tags Where name=?');
			$stmt->execute([$tag]);
			if ($stmt->rowCount() > 0){
			// get tag
				$tag_id = $stmt->fetch();
			// assign tag
				$stmt = $pdo->prepare('Insert Into imagetags (imgid, tagid) Values (?, ?)');
				$stmt->execute([$id, $tag_id['id']]);
			} else {
			// autocreate new tag
				$stmt = $pdo->prepare('Insert Into tags (name) Values (?)');
				$stmt->execute([$tag]);
				$tag_id = $pdo->lastInsertId();
			// assign tag
				$stmt = $pdo->prepare('Insert Into imagetags (imgid, tagid) Values (?, ?)');
				$stmt->execute([$id, $tag_id]);
			}
		}
	}
	
	$contents .= "<p>Successfully <a href=\"show.php?id=$id\">edited</a>!</p>";
	
	// redirect to the show page
	$ht = $_SERVER['HTTP_HOST'];
	header("Location: http://$ht/show.php?id=$id");
	
} else {
	$id_set = ( isset($_GET['id']) ) && ( $_GET['id'] != '' );
	if ($id_set){
		$id = filter_var($_GET['id'], FILTER_SANITIZE_NUMBER_INT);
		$stmt = $pdo->prepare('Select title, thumb, description, upload_time From images Where id=?;');
		$stmt->execute([$id]);
		$image = $stmt->fetch();
		if ($image){
			// blank out sidebar
			$contents = "<style>#sidebar{display: none;}</style>";
			$sidebar = "";
			
			// query image
			$is = $image['thumb'];
			$it = $image['title'];
			$ic = $image['description'];
			$iu = $image['upload_time'];
			
			$ix_ = [];
			$stmt = $pdo->prepare('Select imagetags.tagid As tag_id, tags.name As tag_name From imagetags Inner Join tags On tags.id=imagetags.tagid Where imagetags.imgid=?;');
			$stmt->execute([$id]);
			while ($tag = $stmt->fetch()){
				$tn = $tag['tag_name'];
				array_push($ix_, $tn);
			}
			
			$ix = implode(" ", $ix_);
			
			$contents .= "<form id=\"upload-form\" class=\"std-form\" action=\"edit.php\" method=\"post\">
		<fieldset>
		<input type=\"hidden\" name=\"id\" value=\"$id\" />
		<img src=\"$thumbnail_dir$is\" width=200/>
		</fieldset>
		<fieldset>
		<div class=\"form-row\"><label for=\"upload-title\">Title:</label><input id=\"upload-title\" type=\"text\" name=\"title\" value=\"$it\" /></div>
		<div class=\"form-row\"><label for=\"upload-desc\">Description:</label><textarea id=\"upload-desc\" name=\"desc\">$ic</textarea></div>
		<div class=\"form-row\"><label for=\"upload-tags\">Tags:</label><textarea id=\"upload-tags\" name=\"tags\" required>$ix</textarea></div>
		</fieldset>
		<fieldset>
		<input type=\"submit\" name=\"save\" value=\"Save\" />
		</fieldset>
	    </form>";
		} else {
			$contents .= '<p>Invalid ID!</p>';
		}
	} else {
		$contents .= "<p>Nothing to edit!</p>";
	}
}
// render
require "include/render.php";

?>
