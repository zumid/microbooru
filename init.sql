Create Table `tags` (
  `id`   Int         Not Null Auto_Increment,
  `name` VarChar(64) Not Null Unique,
  Primary Key (`id`)
);

Create Table `images` (
  `id`          Int          Not Null Auto_Increment,
  `imgsrc`      Varchar(512) Not Null,
  `thumb`       Varchar(512) Not Null,
  `title`       Varchar(512) Not Null,
  `description` Varchar(1024) Null,
  `upload_time` Datetime     Not Null Default current_timestamp(),
  Primary Key (`id`)
);

Create Table `imagetags` (
  `imgid`  Int  Not Null,
  `tagid`  Int  Not Null,
  Foreign Key (`imgid`) References `images`(`id`),
  Foreign Key (`tagid`) References `tags`(`id`)
);
