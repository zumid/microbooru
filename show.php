<?php

include_once "include/config.php";
include_once "include/func.php";

$contents = "";

$id_set = ( isset($_GET['id']) ) && ( $_GET['id'] != '' );
if ($id_set){
	$id = filter_var($_GET['id'], FILTER_SANITIZE_NUMBER_INT);
	$stmt = $pdo->prepare('Select title, imgsrc, description, upload_time From images Where id=?;');
	$stmt->execute([$id]);
	$image = $stmt->fetch();
	if ($image){
		$is = $image['imgsrc'];
		$it = $image['title'];
		$ic = $image['description'];
		$iu = $image['upload_time'];
		$contents .= "<img id=\"post-img\" src=\"$fullsize_img_dir$is\"/>";
		$contents .= "<h2 id=\"post-title\">$it</h2>";
		$contents .= "<p id=\"post-description\">$ic</p>";
		
		$stmt = $pdo->prepare('Select imagetags.tagid As tag_id, tags.name As tag_name From imagetags Inner Join tags On tags.id=imagetags.tagid Where imagetags.imgid=?;');
		$stmt->execute([$id]);
		$contents .= "<div id=\"post-tags\"><h3>Tags</h3><ul>";
		while ($tag = $stmt->fetch()){
			$tn = $tag['tag_name'];
			$td = $tag['tag_id'];
			$contents .= "<li><a href=\"/?tags=$tn\">$tn</a></li>";
		}
		$contents .= "</ul></div>";
		
		$sidebar .= "<div id=\"post-stats\"><h3>Stats</h3><ul><li>ID: $id</li><li>Posted on $iu</li></ul></div>";
		$sidebar .= "<div id=\"post-actions\"><h3>Actions</h3><ul><li><a href=\"/edit.php?id=$id\">Edit</a></li><!--li><a href=\"/remove.php?id=$id\">Remove</a></li--></ul></div>";
	} else {
		$contents .= '<p>Invalid ID!</p>';
	}
} else {
	$contents .= "<p>Nothing to show!</p>";
}

// render
require "include/render.php";

?>
