<?php

include_once "include/config.php";
include_once "include/func.php";

// hide sidebar
$contents = "<style>#sidebar{display: none;}</style>";
$sidebar = "";

$expiry =  time() + (20 * 365 * 24 * 60 * 60);

function render_upload_page($tags){
	return "<form id=\"ignore-form\" class=\"std-form\" action=\"set_ignore.php\" method=\"post\">
        <fieldset>
        <div class=\"form-row\"><label for=\"upload-tags\">Manage ignorelist: </label></div>
        <textarea id=\"upload-tags\" name=\"tags\">$tags</textarea>
        </fieldset>
        <fieldset>
        <input type=\"submit\" name=\"save\" value=\"Save\" />
        </fieldset>
    </form>
    <p>This will be stored in your cookies.</p>
    <p>To clear your ignorelist, simply blank out the text field or clear your cookies.</p>
    ";
}

if (isset($_POST['tags'])){
	$pt = $_POST['tags'];
	$contents .= display_confirmation('Ignorelist set!');
	setcookie('ignorelist', $pt, [
		'expires' => $expiry,
		'path' => '/',
		'samesite' => 'Lax',	
	]);
	$contents .= render_upload_page($pt);
} else {
	$contents .= render_upload_page($_COOKIE['ignorelist']);
}

// render
require "include/render.php";

?>
