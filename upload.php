<?php

include_once "include/config.php";
include_once "include/func.php";


/*echo "<pre>";
print_r($_FILES);
print_r($_POST);
echo "</pre>";
*/

// hide sidebar
$contents = "<style>#sidebar{display: none;}</style>";
$sidebar = "";

function render_success_page($id){
	return '<p>Congratulations! Your image has been successfully <a href="show.php?id='.$id.'">uploaded</a>!</p>';
}

function render_upload_page($assets_dir, $post){
	return '<form id="upload-form" class="std-form" action="upload.php" method="post" enctype="multipart/form-data">
        <fieldset>
        <div class="form-row"><label for="upload-img">Choose a file:</label><input id="upload-img" type="file" name="image"/></div>
        <img id="preview" src="'.$assets_dir.'PlaceholderForArt.png" width="400" />
        </fieldset>
        <fieldset>
        <div class="form-row"><label for="upload-title">Title:</label><input id="upload-title" type="text" name="title" value="'.$post['title'].'" /></div>
        <div class="form-row"><label for="upload-desc">Description:</label><textarea id="upload-desc" name="desc"></textarea></div>
        <div class="form-row"><label for="upload-tags">Tags:</label><textarea id="upload-tags" name="tags" required>'.$post['tags'].'</textarea></div>
        </fieldset>
        <fieldset>
        <input type="submit" name="upload" value="Upload" />
        </fieldset>
    </form><script>
	document.getElementById("upload-img").onchange = function(){
	    var read = new FileReader();
	    read.onload = function(e){
		document.getElementById("preview").src = e.target.result;
	    };
	    read.readAsDataURL(this.files[0]);
	}</script>';
}

if(isset($_FILES['image'])) {
	$fileError = $_FILES["image"]["error"];
	switch($fileError){
		case UPLOAD_ERR_INI_SIZE:
			$contents .= display_warning("Image size exceeds limits!");
			$contents .= render_upload_page($assets_dir, $_POST);
			break;
		
		case UPLOAD_ERR_NO_FILE:
			$contents .= display_warning("No file uploaded!");
			$contents .= render_upload_page($assets_dir, $_POST);
			break;
		
		case UPLOAD_ERR_OK:
			$required_fields = ( $_POST['tags'] != '' );
			if (!($required_fields)){
				$contents .= display_warning("Tags must be set!");
				$contents .= render_upload_page($assets_dir, $_POST);
			} else {
				if (preg_match('/[.](jpg)|(gif)|(png)$/', $_FILES['image']['name'])) {
					$filename = generate_random_string(6).'-'.$_FILES['image']['name'];
					$source = $_FILES['image']['tmp_name'];   
					$target = $fullsize_img_dir.$filename;
					move_uploaded_file($source, $target);
					create_thumbnail($fullsize_img_dir, $thumbnail_dir, $filename, $thumbnail_width);    
					
					$stmt = $pdo->prepare('Insert Into images(imgsrc, thumb, title, description) Values (?, ?, ?, ?);');
					$stmt->execute([$filename, $filename, $_POST['title'], $_POST['desc']]);
					$id = $pdo->lastInsertId();
					$contents .= render_success_page($id);
					
					// will take a shitton of queries, it's a hack
					// assume all the required fields are set
					
					// split tag query into tokens
					$tags = array_unique(explode(" ", $_POST['tags']));
					
					// remove blank query
					while (($key = array_search("", $tags)) !== false) {
						unset($tags[$key]);
					}
					
					foreach ($tags as $tag){
						// sanitize each string
						$tag = filter_var($tag, FILTER_SANITIZE_MAGIC_QUOTES);
						// ignore all the ones with special characters
						if (substr($tag, 0, 1) == "~" ){}
						else if (substr($tag, 0, 1) == "-" ){}
						else {
						// process tag
							$stmt = $pdo->prepare('Select id From tags Where name=?');
							$stmt->execute([$tag]);
							if ($stmt->rowCount() > 0){
							// get tag
								$tag_id = $stmt->fetch();
							// assign tag
								$stmt = $pdo->prepare('Insert Into imagetags (imgid, tagid) Values (?, ?)');
								$stmt->execute([$id, $tag_id['id']]);
							} else {
							// autocreate new tag
								$stmt = $pdo->prepare('Insert Into tags (name) Values (?)');
								$stmt->execute([$tag]);
								$tag_id = $pdo->lastInsertId();
							// assign tag
								$stmt = $pdo->prepare('Insert Into imagetags (imgid, tagid) Values (?, ?)');
								$stmt->execute([$id, $tag_id]);
							}
						}
					}
					
					
				} else {
					$contents .= display_warning("Image must be a jpg, gif, or png!");
					$contents .= render_upload_page($assets_dir, $_POST);
				}
			}
			break;
		
		default:
			$contents .= render_upload_page($assets_dir, $_POST);
			break;
	}
} else {
	// nothing
	$contents .= render_upload_page($assets_dir, $_POST);
}
// render
require "include/render.php";
?>
